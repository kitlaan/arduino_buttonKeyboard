# buttonKeyboard
Arduino code for generating keyboard presses from buttons.

# Notes
This uses the 'leonardo' equivalent chips to do the heavy lifting.

As written, 5 buttons are available that trigger either a momentary
or toggle'd key press.

# TODO
* serial interface to reconfigure the keys pressed
* finish testing debounce
