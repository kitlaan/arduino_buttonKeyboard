/**
 * Button Controller
 * USB HID (macro) controller with multiple buttons.
 */

#include <Arduino.h>
#include <Keyboard.h>

#define BOOLTOPIN(x) ((x) ? HIGH : LOW)

#define DEBOUNCE_MS (5)

struct buttonState
{
  byte buttonPin;
  byte ledPin;

  byte keyPress;
  byte option;

  bool mode;
  byte currState;
  byte lastState;
  unsigned long lastTime;
} buttons[] =
{
  {10, 9, '5', 1, false, LOW, LOW, 0},
  {16, 7, '6', 1, false, LOW, LOW, 0},
  {14, 6, '7', 1, false, LOW, LOW, 0},
  {15, 4, '0', 1, false, LOW, LOW, 0},
  {18, 8, '8', 0, false, LOW, LOW, 0},
  {19, 5, '9', 0, false, LOW, LOW, 0},
};

#define NUM_BUTTONS (sizeof(buttons)/sizeof(buttons[0]))

// true if we have a jammed key at start
bool failSafeMode = false;

// true if we're in programming mode
bool programMode = false;

void debounceButton(struct buttonState &button);
void handleButton(struct buttonState &button);

void runFailSafeMode();
void runProgramMode();

void setup()
{
  for (size_t i = 0; i < NUM_BUTTONS; i++)
  {
    pinMode(buttons[i].buttonPin, INPUT_PULLUP);
    pinMode(buttons[i].ledPin, OUTPUT);

    if (digitalRead(buttons[i].buttonPin) == LOW)
    {
      failSafeMode = true;
    }
  }

  if (!failSafeMode)
  {
    Keyboard.begin();
  }
}

void loop()
{
  if (failSafeMode)
  {
    runFailSafeMode();
    return;
  }

  if (programMode)
  {
    runProgramMode();
    return;
  }

  // buttons!
  for (size_t i = 0; i < NUM_BUTTONS; i++)
  {
    debounceButton(buttons[i]);
  }
}

void debounceButton(struct buttonState &button)
{
  byte reading = digitalRead(button.buttonPin);

  if (reading != button.lastState)
  {
    button.lastTime = micros();
    button.lastState = reading;
  }
  else if (micros() - button.lastTime > (DEBOUNCE_MS * 1000))
  {
    if (reading != button.currState)
    {
      button.currState = reading;

      // we're edge triggered, so let's do something!
      handleButton(button);
    }
  }
}

void handleButton(struct buttonState &button)
{
  bool doAction = false;

  if (button.option == 1)
  {
    // toggle mode (aka action only on HIGH->LOW)
    if (button.currState == LOW)
    {
      button.mode = !button.mode;
      doAction = true;
    }
  }
  else
  {
    // momentary mode (aka action while pressed)
    button.mode = (button.currState == LOW);
    doAction = true;
  }

  if (doAction)
  {
    digitalWrite(button.ledPin, BOOLTOPIN(button.mode));

    if (button.mode)
    {
      Keyboard.press(button.keyPress);
    }
    else
    {
      Keyboard.release(button.keyPress);
    }
  }
}

void runFailSafeMode()
{
  // when in fail-safe mode, blink the LEDs
  for (size_t i = 0; i < NUM_BUTTONS; i++)
  {
    buttons[i].mode = !buttons[i].mode;
    digitalWrite(buttons[i].ledPin, BOOLTOPIN(buttons[i].mode));
  }
  delay(200);
}

void runProgramMode()
{
  // TODO
}
